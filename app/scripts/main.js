
var app = angular.module('BackpackApp', ["ngRoute", "firebase"]);

// Routing
	app.config(function($routeProvider, $locationProvider) {
		$routeProvider
		.when("/", {
			templateUrl	: "views/login.html",
			controller	: "loginController"
		})
		.when("/newsfeed", {
			templateUrl : "views/newsfeed.html",
			controller : "newsfeedController"
		})
		.when("/compose", {
			templateUrl : "views/compose.html",
			controller : "composeController"
		});

	});

// Controllers

	// Databases
	var users 		= new Firebase('https://devchat-d1fab.firebaseio.com/users/');
	var newsfeed	= new Firebase('https://devchat-d1fab.firebaseio.com/newsfeed/');

	app.controller("loginController", function($scope, $location, account, validify, convert, lz) {

		if(account.isLogin())
			$location.path('/newsfeed')

		$scope.toggleSignInView = function() {
			angular.element('.sign_in_holder').fadeToggle();
		}

		$scope.si_errors = null;
		$scope.submitSignIn = function() {
			validify.open();
			validify.set_rules('username', $scope.si_username, 'required');
			validify.set_rules('password', $scope.si_password, 'required');

			if(validify.run()) {
				users.child($scope.si_username).once('value', function(snapshot) {
                    if(snapshot.val() != null) {
                        snapshot.forEach(function(userSnap) {
                            var data = userSnap.val();
                            if($scope.si_password == lz.decompress(data.password)) {
                                localStorage.setItem('user', JSON.stringify(data));
                                $location.path("/newsfeed");
                            } else {
								validify.err('password', "Password doesn't match in the record");
							}
                        });
                    } else {
						validify.err("username", "Username doesn't exist.");
					}
                });

				$scope.si_errors = validify.errors();
			}

			$scope.si_errors = validify.errors();
		}

		$scope.su_errors = null;
		$scope.submitSignUp = function() {
			validify.open();

			var su = $scope.reg;
			validify.set_rules('first name', su.fname, 'required');
			validify.set_rules('last name', su.lname, 'required');
			validify.set_rules('email', su.email, 'required|email');
			validify.set_rules('username', su.username, 'required');
			validify.set_rules('password', su.password, 'required');

			if(validify.run()) {
				users.child(su.su_username).once('value', function(snapshot) {
					if(snapshot.val() == null){
						su.password = lz.compress(su.password);
						firebase.database().ref().child('users').push(su);
						localStorage.setItem('user', angular.toJson(su));
						$location.path("/newsfeed");
					} else {
						validify.err("username", "Username already exist.");
					}
				});

				$scope.su_errors = validify.errors();
			}

			$scope.su_errors = validify.errors();
		}
	})

	.controller('newsfeedController', function($scope, $location, $firebaseObject, account) {
		if(!account.isLogin())
			$location.path("/");

		var ref = firebase.database().ref().child('newsfeed');
		$scope.newsfeed = $firebaseObject(ref);
	})

	.controller('composeController', function($scope, $location, account, lz, validify, convert) {
		$scope.post = function() {
			$scope.compose.timestamp = new Date();
			var file = document.querySelector('input[type=file]').files[0];

			var reader  = new FileReader();
			var result = "";
			reader.addEventListener("load", function () {
				 $scope.compose.photo = reader.result;
			}, false);
			reader.readAsDataURL(file);

			var check = setInterval(function() {
				validify.open();
				validify.set_rules('title', $scope.compose.title, 'required');
				validify.set_rules('location', $scope.compose.location, 'required');
				validify.set_rules('photo', $scope.compose.photo, 'required');
				validify.set_rules('content', $scope.compose.content, 'required');
				validify.set_rules('tags', $scope.compose.tags, 'required');

				if(validify.run()) {
					$scope.compose.username = account.row('username');
					$scope.compose.content 	= lz.compress($scope.compose.content);
					$scope.compose.photo 	= lz.compress($scope.compose.photo);
					firebase.database().ref().child('newsfeed').push($scope.compose);
					$location.path("/newsfeed");
					window.clearInterval();
				}

				window.clearInterval();
			}, 1000)
		}
	});

// Filters

	app.filter('decompress', function() {
		return function(x) {
			return "asd";
		};
	});

// Services

	// CRUD
		app.service('query', function() {
			this.get = function(db, pk) {
				db.child(pk).once('value', function(snapshot) {
					if(snapshot.val() !== null){
						snapshot.forEach(function(userSnap) {
							return userSnap.val();
						});
					} else {
						return null;
					}
				});
			}
		})
	// End CRUD

	var rules 	= {};
	var valid 	= false;
	var err 	= {};

	app.service('validify', function() {
		// Piece by piece rules.
		this.set_rules = function(model, value, r) {
			rules[model] = {
				value	: value,
				rules	: r
			};
		}

		// Migrate the whole obj to rules
		this.config = function(obj) {
			$.each(obj, function(k , v) {
					rules[k] = v;
			});
		}

		// Validate every rule
		this.run = function() {
			var f = 0;

			$.each(rules, function(k, v) {
				var r = v.rules.split('|');
				f += r.length;
				$.each(r, function(k2, v2) {

					/*
						Checks if the rule is valid
					*/
					if(eval(v2)(k, v.value))
						f--;
				});
			});

			return f == 0 ? true : false;
		}

		// Get all the form error.
		this.errors = function() {
			return err;
		}

		// Get a specific error field
		this.error = function(field) {
			return err[field] === undefined ? "" : err[field];
		}

		this.err = function(field, str) {
			err[field] = str;
		}

		// ReInitialize the Validation
		this.open = function() {
			rules 	= {};
			err 	= {};
		}

		// Below are the rules of the valiation
		var required = function(field, str) {
			if(str == undefined || str == "") {
				err[field] = "The " + field + " field is required.";
				return false;
			}

			return true;
		}

		var email = function(field, str) {
			str = unescape(str);
	        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if(!regex.test(str)) {
				err[field] = "The " + field + " field must be a valid e-mail.";
				return false;
			}

			return true;
		}
		// end of rules
	});

	app.service('lz', function() {
		this.compress = function(str) {
			return LZString.compressToUTF16(str);
		}

		this.decompress = function(str) {
	        return LZString.decompressFromUTF16(str);
		}
	});

	app.service('convert', function() {
		this.SerializeToObject = function(data) {
			var arr = data.split('&');
	        var raw = {};
	        for(var i = 0; i < arr.length; i++) {
	            var d = arr[i].split('=');
	            raw[d[0]] = d[1];
	        }

	        return raw;
		}

	});

	app.service('account', function() {
		this.isLogin = function() {
			return localStorage.getItem('user') != null ? true : false;
		}

		this.logout = function() {
			localStorage.removeItem('user');
		}

		this.row = function(field) {
			var data = angular.fromJson(localStorage.getItem('user'));
			return data[field];
		}

		this.result = function() {
			return angular.fronJson(localStorage.getItem('user'));
		}
	})

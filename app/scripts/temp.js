$(document).ready(function() {
    var base_url = 'http://hairasia.ph/co/';
	var redirect = '/';
    var params = parseURLParams(window.location.href);
    var current = params == null || params.channel == null ? 'general/' : params.channel  + '/';

    var title = params == null || params.channel == null ? 'General' : params.channel;

    $('#channel_title').text(title);

    var channels = new Firebase('https://devchat-d1fab.firebaseio.com/channel/');
    var channel = new Firebase('https://devchat-d1fab.firebaseio.com/channel/' + current);

    var users = new Firebase('https://devchat-d1fab.firebaseio.com/users/');

    var username = null;
    var user = null;
    var ip = '';
    var place = '';

    // Get Username
    $.ajax({
        type : 'POST',
        url  : 'http://hairasia.ph/co/server/app.php',
        async : false,
        success : function(data) {
            ip = data;
        }
    });

    if(localStorage.getItem('user') != null) {
        user = $.parseJSON(localStorage.getItem('user'));
        username = user.username;
    } else {
        username = null;
        if(window.location.href.indexOf('newsfeed.html') >= 1) {
            window.location = redirect + 'index.html';
        }
    }

    if(username != null) {
        channel.orderByChild('timestamp').limitToLast(20).on('child_added', function(snapshot) {
            var data = snapshot.val();

            if(data.from == username)
                return false;

            var source      = $('#tempConvo').html();
            var template    = Handlebars.compile(source);
            var sender      = username == data.username ? 'me' : (data.ip == null && data.username == null? 'announce' : '');
            var msg         = decompress(data.msg);
            var time        = timeSince(data.timestamp);
            var placed       = data.location != null ? ' · <small>'+ data.location +'</small>' : '';
            var context     = {sender : sender, message : msg, key : snapshot.key(), ago: time, from : data.username, place : placed}
            var html        = template(context);

            $('#convo_holder').append(html);
            $('.platform-body').animate({ scrollTop: $('.platform-body').prop('scrollHeight')*3 }, 'slow');
        });
    }

    channels.on('child_added', function(snapshot) {
        $('#load_existing_channel').append('<li><a href="'+ base_url +'?channel='+ snapshot.key().replace(/ /g, '+') +'">'+ snapshot.key() +'</a></li>');
    });

    if(localStorage.getItem('curr') != title) {
        localStorage.setItem('curr', title);
        announce(compress(username + ' has entered the Channel.'), username);
    }

    $('#send_convo').click(function() {
        if ($('textarea').val().replace(/\n/g, '') != '') {
            var msgC     = compress($('textarea').val());
            speak(msgC, username, ip);
            $('textarea').val('');
            $('.platform-body').animate({ scrollTop: $('.platform-body').prop('scrollHeight') }, 'slow');
        }
    });

    // Front End
        $('#toggleMenu').click(function() {
            $('.platform-nav').removeClass('bounceOutDown');
            $('.platform-nav').show().addClass('bounceInDown');
        });

        $('#closeMenu').click(function() {
            $('.platform-nav').removeClass('bounceInDown');
            $('.platform-nav').addClass('bounceOutDown');
        });

        $('#main_nav li').click(function() {
            if($(this).find('ul').length != 0) {
                $(this).find('ul').slideToggle();
            }
        });

    // Sign Up
        $('#sign_up_btn').click(function(e) {
            e.preventDefault();

            var data = $('#sign_up_form').serialize();
            var raw = SerializeToObject(data);


            if(validateObject(raw)) {
                users.child(raw.username).once('value', function(snapshot) {
                    if(snapshot.val() == null){
                        raw.password = compress(raw.password);
                        users.child(raw.username).push(raw);
                        $('#sign_up_form input[name=\'username\']').css('border', '1px solid rgb(204, 204, 204)').parent().find('small').remove();
                        localStorage.setItem('user', raw);
                        window.location = redirect + 'newsfeed.html';
                    } else {
                        $('#sign_up_form input[name=\'username\']').css('border', '1px solid red').parent().append('<small style=\'color: red;\'>Username already exist.</small>');
                    }
                });
            } else {
                console.log('Error!');
            }
        });
    // Sign In
        $('#close_sign_in').click(function(e) {
            e.preventDefault();
            $('.sign_in_holder').removeClass('bounceInUp').addClass('bounceOutDown');
        });

        $('#sign_in_show').click(function(e) {
            $('.sign_in_holder').removeClass('bounceOutDown').show().addClass('bounceInUp');
        });

        $('#sign_in_btn').click(function(e) {
            e.preventDefault();

            var data = $('#sign_in_form').serialize();
            var raw = SerializeToObject(data);


            if(validateObject(raw)) {
                users.child(raw.username).once('value', function(snapshot) {
                    if(snapshot.val() != null){
                        snapshot.forEach(function(userSnap) {
                            var data = userSnap.val();
                            if(raw.password == decompress(data.password)) {
                                $('#sign_in_form  input[name=\'username\']').css('border', '1px solid rgb(204, 204, 204)').parent().find('small').remove();
                                $('#sign_in_form  input[name=\'password\']').css('border', '1px solid rgb(204, 204, 204)').parent().find('small').remove();
                                localStorage.setItem('user', JSON.stringify(data));
                                window.location = redirect + 'newsfeed.html';
                            } else {
                                $('#sign_in_form input[name=\'password\']').css('border', '1px solid red').parent().append('<small style=\'color: red;\'>Password doesn\'t match.</small>');
                            }
                        });
                    } else {
                        $('#sign_in_form input[name=\'username\']').css('border', '1px solid red').parent().append('<small style=\'color: red;\'>Username doesn\'t exist.</small>');
                    }
                });
            } else {
                console.log('Error!');
            }
        });

    function validateObject(serialize) {
        var c = $.map(serialize, function(n, i) { return i; }).length;
        $.each(serialize, function(k, v) {
            var rules = $('input[name=\''+ k +'\']').attr('data-rules');
            var rls = rules.split('|');
            var vl = 0;
            $.each(rls, function(kr, r) {
                switch (r) {
                    case 'required':
                            if(v != '') {
                                 $('input[name=\''+ k +'\']').css('border', '1px solid rgb(204, 204, 204)');
                                 vl++;
                            } else {
                                $('input[name=\''+ k +'\']').css('border', '1px solid red');
                            }
                        break;
                    case 'email':
                        if(isValidEmailAddress(v)) {
                             $('input[name=\''+ k +'\']').css('border', '1px solid rgb(204, 204, 204)');
                             vl++;
                        } else {
                            $('input[name=\''+ k +'\']').css('border', '1px solid red');
                        }
                        break;
                    default:
                }


            });
            c = rls.length == vl ? c-1 : c;
        })

        return c == 0 ? true : false;
    }

    function isValidEmailAddress(emailAddress) {
        emailAddress = unescape(emailAddress);
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(emailAddress);
    };

    function SerializeToObject(data) {
        var arr = data.split('&');
        var raw = {};
        for(var i = 0; i < arr.length; i++) {
            var d = arr[i].split('=');
            raw[d[0]] = d[1];
        }

        return raw;
    }

    // Send Message to Current Channel
    function speak(message, userid, ip) {
        channel.push({
            username    : userid,
            msg         : message,
            ip          : ip,
            location    : place != '' ? place : null,
            timestamp   : Math.floor(Date.now() / 1000),
        });
    }

    function announce(message, from) {
        channel.push({
            from    : from,
            msg     : message,
            timestamp : Math.floor(Date.now() / 1000),
        });
    }

    function compress(str) {
        return LZString.compressToUTF16(str);
    }

    function decompress(str) {
        return LZString.decompressFromUTF16(str);
    }

    // Get URL Parameters
    function parseURLParams(url) {
        var queryStart = url.indexOf('?') + 1,
            queryEnd   = url.indexOf('#') + 1 || url.length + 1,
            query = url.slice(queryStart, queryEnd - 1),
            pairs = query.replace(/\+/g, ' ').split('&'),
            parms = {}, i, n, v, nv;

        if (query === url || query === '') return;

        for (i = 0; i < pairs.length; i++) {
            nv = pairs[i].split('=', 2);
            n = decodeURIComponent(nv[0]);
            v = decodeURIComponent(nv[1]);

            if (!parms.hasOwnProperty(n)) parms[n] = [];
            parms[n].push(nv.length === 2 ? v : null);
        }
        return parms;
    }

    getLocation();

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        }
    }

    function showPosition(position) {
        var lat = position.coords.latitude;
        var lon = position.coords.longitude;

        $.ajax({
            url : 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+ lat +','+ lon,
            dataType : 'json',
            success : function(data) {
                place = data.results[1].address_components[0].short_name;
            }
        })

    }

    function timeSince(date) {
        var date = new Date(date*1000);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }


});
